package com.dip.bootcamp.utilities;

public class InformationConstant {

    public static final String websiteTitle = " || RapidChain Technology";
    public static final String DB_SCHEMA_NAME = "SYSTEM";
    public static final String REF_CURSOR_RECORDSET = "p_recordset";

}
