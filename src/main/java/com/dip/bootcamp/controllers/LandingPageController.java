package com.dip.bootcamp.controllers;

import com.dip.bootcamp.utilities.InformationConstant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/landing")
public class LandingPageController {
    @GetMapping(value = (""))
    public String dashboard(Model model){
        String pageTitle = "Dashboard" + InformationConstant.websiteTitle;

        model.addAttribute("username", "Nanra");
        model.addAttribute("title", pageTitle);
        return "landing-page/index";
    }
}
